####### 2 - LA METHODE DES K-PLUS PROCHES VOISINS ######

# chargement jeu de donnees mnist
from sklearn.datasets import fetch_openml
from sklearn import model_selection, neighbors
import numpy as np
from time import time
mnist = fetch_openml('mnist_784')

# creation echantillon de 5000 donnees (prise aleatoirement)
data = []
target = []
for i in range(5000):
    index = np.random.randint(70000)
    data.append(mnist.data[index])
    target.append(mnist.target[index])
#print(len(data), len(target), data[0], target[0])   # check

# division de l'echantillon en un set de training et un set de testing
data_train, data_test, target_train, target_test = model_selection.train_test_split(data, target,train_size=0.8)
#print(len(xtrain), len(xtest)) # check

i = input("\n--- [SKIP MENU]--- \n1- Exercice 2 : la méthode des k-nn (premier classifieur)\n2 - Faire varier le nombre de voisins (k) de 2 jusqu’à 15 et afficher le score\n3 - Faire varier le pourcentage des échantillons (training et test) et afficher le score\n4 - Faire varier les types de distances (p)\n5 - Fixer n_job à  1 puis à -1 et calculer le temps dans chacun\n> ")

if (i == '1'):
    # creation classifieur k-nn
    clf = neighbors.KNeighborsClassifier(10)
    print("--- entrainement ... ---")
    clf.fit(data_train, target_train)
    print("--- [DONE] ---")

    # prediction de l'image 4
    print("\n\n--- [PREDICTION] (image 4) ---")
    print("classe reelle : "+str(target_test[4]))
    classe_pred = clf.predict([data_test[4]])
    print("classe predite : "+str(classe_pred))

    # affichage score sur l'echantillon de test, puis de train
    print("\n\n--- [SCORE] ---")
    score = clf.score(data_test, target_test)
    print("sur echantillon de test : "+str(score)+" ("+str(score*100)[:4]+"%)")
    score = clf.score(data_train, target_train)
    print("sur echantillon de train : {0} ({1}%) (taux d'erreur {2}%)".format(score, str(score*100)[:4], str((1 - score)*100)[:4]+"%"))

if (i == '2'):
    print("\n\n--- [TEST DE VARIATION] ---")
    print("test sur k : on fait varier k entre 2 et 15")
    for k in range(2, 16):
        clf = neighbors.KNeighborsClassifier(k)
        f = model_selection.KFold(n_splits=10, shuffle=True)
        score = []
        data = np.asarray(data)
        target = np.asarray(target)
        for train_index, test_index in f.split(data):
            data_train = data[train_index]
            data_test = data[test_index]
            target_train = target[train_index]
            target_test = target[test_index]
            clf.fit(data_train, target_train)
            score.append(clf.score(data_test, target_test))
        print("score pour k = {0} : {1} ({2}%)".format(k, np.mean(score), str(np.mean(score)*100)[:4]))

if (i == '3'):
    print("\n\n--- [TEST DE VARIATION] ---")
    print("test sur la division de l'echantillon N : on fait varier N entre 10% et 90%")
    print("(on set k nombre de voisins à 10)")
    clf = neighbors.KNeighborsClassifier(10)
    for k in range(1, 10):
        #f = model_selection.KFold(n_splits=10, shuffle=True)
        data_train, data_test, target_train, target_test = model_selection.train_test_split(data, target,train_size=k/10)
        clf.fit(data_train, target_train)
        score = clf.score(data_test, target_test)
        print("score pour N = {0}% : {1} ({2}%)".format(str(k*10), score, str(score*100)[:4]))

if (i == '4'):
    print("\n\n--- [TEST DE VARIATION] ---")
    print("test sur la distance p : on fait varier p entre 1 et et 3 (le defaut etant 2)")
    print("(on set k nombre de voisins à 10, et N rapport echantillon train / test a 80%)")
    for p in range(1, 4):
        clf = neighbors.KNeighborsClassifier(10, p=p)
        #f = model_selection.KFold(n_splits=10, shuffle=True)
        data_train, data_test, target_train, target_test = model_selection.train_test_split(data, target,train_size=0.8)
        clf.fit(data_train, target_train)
        score = clf.score(data_test, target_test)
        print("score pour p = {0} : {1} ({2}%)".format(p, score, str(score*100)[:4]))

if (i == '5'):
    print("\n\n--- [TEST DE VARIATION] ---")
    print("test sur la distance de n_job : on fixe n_job a 1 (one processor) puis a -1 (all processors)")
    print("(on set k nombre de voisins à 10, et N rapport echantillon train / test a 80%, et p à 2)")
    data_train, data_test, target_train, target_test = model_selection.train_test_split(data, target,train_size=0.8)
    clf = neighbors.KNeighborsClassifier(10, n_jobs=1)
    print("\nn_job = 1\nlancement entrainement ...")
    t1 = time()
    clf.fit(data_train, target_train)
    score = clf.score(data_test, target_test)
    t2 = time()
    print("[END] T = "+str(t2-t1)[:4]+"s")
    print("score pour n_job = 1 : {0} ({1}%)".format(score, str(score*100)[:4]))

    clf = neighbors.KNeighborsClassifier(10, n_jobs=-1)
    print("\nn_job = -1\nlancement entrainement ...")
    t1 = time()
    clf.fit(data_train, target_train)
    score = clf.score(data_test, target_test)
    t2 = time()
    print("[END] T = "+str(t2-t1)[:4]+"s")
    print("score pour n_job = 1 : {0} ({1}%)".format(score, str(score*100)[:4]))
