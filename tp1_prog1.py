####### 1 - JEUX DE DONNES ######

# --- [1] --- #
from sklearn.datasets import fetch_openml

# importation jeu de données MNIST
mnist = fetch_openml('mnist_784')

# affichage mnist (dictionnaire)
print('--- mnist ---\n'+str(mnist)+'\n')

# affichage mnist.data (array)
print('--- mnist.data ---\n'+str(mnist.data)+'\n')

# affichage mnist.target (label => a quoi l'image correspond)
print('--- mnist.target ---\n'+str(mnist.target)+'\n')


# len mnist.data (combien il ya d'images)
print('--- len(mnist.data) ---\n'+str(len(mnist.data))+'\n')

# mnist.data.shape (len du data + nb pixel de l'image)
print('--- mnist.data.shape ---\n'+str(mnist.data.shape)+'\n')

# mnist.target.shape (len du target + taille d'un target (ici 1 donc rien))
print('--- mnist.target.shape ---\n'+str(mnist.target.shape)+'\n')

# mnist.data[0] (data correspondant a la premiere image)
print('--- mnist.data[0] ---\n'+str(mnist.data[0])+'\n')

# mnist.data[0][1] (data correspondant au 1er pixel de la premiere image)
print('--- mnist.data[0][1] ---\n'+str(mnist.data[0][1])+'\n')

# mnist.data[:,1] (pour chaque image, 1ere colonne)
print('--- mnist.data[:,1] ---\n'+str(mnist.data[:,1])+'\n')

# mnist.data[:100] (100 premieres images)
print('--- mnist.data[:100] ---\n'+str(mnist.data[:100])+'\n')

# --- [2] --- #
# affichage de la 1ere image avec matplotlib (un 5)
from sklearn import datasets
import matplotlib.pyplot as plt
images = mnist.data.reshape((-1, 28, 28))
plt.imshow(images[0], cmap=plt.cm.gray_r, interpolation="nearest")
plt.show()

# affichage classe (confirmation de 5)
print(mnist.target[0])
